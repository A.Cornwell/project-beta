from django.shortcuts import render
from django.shortcuts import render
from .models import Technician, Appointment, AutomobileVO
from django.http import JsonResponse
import json
from django.views.decorators.http import require_http_methods
from .encoders import AppointmentEncoder, TechnicianEncoder


@require_http_methods(["GET", "POST"])
def list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technician": technicians},
            encoder = TechnicianEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        technician = Technician.objects.create(**content)
        return JsonResponse(
            technician,
            encoder = TechnicianEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE"])
def show_technicians(request, pk):
    if request.method == "GET":
        technicians = Technician.objects.get(id=pk)
        return JsonResponse(
            technicians,
            encoder=TechnicianEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Technician.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})



@require_http_methods(["GET", "POST"])
def list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder = AppointmentEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        try:
            technician = Technician.objects.get(pk=content["technician"])
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid technician id"},
                status=400
            )
        try:
            vin_num = AutomobileVO.objects.get(vin=content["vin"])
            content["vin"] = vin_num
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid vin number"},
                status=400
            )
        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder = AppointmentEncoder,
            safe=False
        )


@require_http_methods(["GET", "DELETE"])
def show_appointment(request, pk):
    if request.method == "GET":
        appointment = Appointment.objects.get(id=pk)
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Appointment.objects.filter(pk=pk).delete()
        return JsonResponse({"deleted": count > 0})
