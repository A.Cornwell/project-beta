from common.json import ModelEncoder
from .models import Appointment, Technician, AutomobileVO


class AutomobileVOEncoder(ModelEncoder):
        model = AutomobileVO
        properties = ["vin"]


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = ["name","employee_num", "pk",]


class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = ["vin","name", "date_time", "reason_for_service", "technician", "pk"]

    encoders = {
        "vin": AutomobileVOEncoder(),
        "technician": TechnicianEncoder(),
    }
