from django.db import models

# Create your models here.


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)

    def __str__(self):
        return self.vin


class Technician(models.Model):
    name = models.CharField(max_length=200)
    employee_num = models.IntegerField()

    def __str__(self):
        return self.name


class Appointment(models.Model):
    name = models.CharField(max_length=200)
    date_time = models.DateTimeField()
    reason_for_service = models.TextField()

    technician = models.ForeignKey(
        Technician,
        related_name = "appointments",
        on_delete = models.CASCADE,
    )

    vin = models.ForeignKey(
        AutomobileVO,
        related_name = "appointments",
        on_delete = models.CASCADE,
    )

    def __str__(self):
        return self.name
