import { useState, useEffect} from 'react'

const SaleRecordList = () => {
    const [salerecords, setSaleRecords] = useState([])
    const [salesperson, setSalesPerson] = useState([])
    const [FilterName, setFilterName] = useState("");

    const getData = async () => {
        const resp = await fetch('http://localhost:8090/api/salerecords/');
        const resp2 = await fetch('http://localhost:8090/api/salespersons/');
        if (resp.ok && resp2.ok) {
            const data = await resp.json()
            const data2 = await resp2.json()
            setSaleRecords(data.salerecords)
            setSalesPerson(data2.salesperson)
        }
    }

    useEffect(()=> {
        getData()
    }, [])

    const handleChange = (e) => {
            setFilterName(e.target.value);
    };
    return <>
        <div className="row">
            <div className="offset-3 col-9">
                <div className="shadow p-6 mt-5">
                    <h1>Sale Records</h1>
                        <select key={salesperson.id} onChange={handleChange}>
                        <option value={"default"}>
                        Select a Sales Person
                        </option>
                        {salesperson.map(salesperson => {return (<option key={salesperson.id}>{ salesperson.name }</option>);})}
                        </select>
                    <table className="table table-striped">
                        <thead>
                            <tr>
                                <th>Sales Person</th>
                                <th>Customer</th>
                                <th>VIN</th>
                                <th>Sale Price</th>
                            </tr>
                        </thead>

                            {(() => {
                            if ((FilterName === "") || (FilterName === "default")) {
                            return (
                                <tbody>
                                {salerecords.map(salerecord => {
                                    return (
                                        <tr key={salerecord.id}>
                                            <td>{ salerecord.sales_person.name }</td>
                                            <td>{ salerecord.customer.name }</td>
                                            <td>{ salerecord.automobile.vin }</td>
                                            <td>${ salerecord.sale_price }</td>
                                        </tr>
                                    );
                                })}

                                </tbody>
                            )
                            }
                            else {
                            return (
                                <tbody>
                                {salerecords.filter(salerecord => salerecord.sales_person.name === FilterName).map(salerecord => {
                                    return (
                                        <tr key={salerecord.id}>
                                            <td>{ salerecord.sales_person.name }</td>
                                            <td>{ salerecord.customer.name }</td>
                                            <td>{ salerecord.automobile.vin }</td>
                                            <td>${ salerecord.sale_price }</td>
                                        </tr>
                                    );
                                })}

                        </tbody>
                            )
                            }
                        })()}
                    </table>
                </div>
            </div>
        </div>
    </>
}

export default SaleRecordList;
