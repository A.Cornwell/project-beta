import { NavLink } from 'react-router-dom';
import './Nav.css';


function Nav() {


  return (
    <nav className="navbar navbar-expand-lg navbar-dark ">
      <div className="container">
        <NavLink className="navbar-brand " to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav mb-2 mb-lg-0 ms-auto">
          <li className="nav-item">
              <NavLink className="nav-link" aria-current="page" to="/">Home</NavLink>
            </li>
          <li className="nav-item dropdown">
          <button className="btn btn-dark dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
            Inventory
          </button>
          <ul className="dropdown-menu dropdown-menu-dark">
          <li className="nav-item">
              <NavLink className="nav-link" aria-current="page" to="/manufacturers">Manufacturer</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" aria-current="page" to="/manufacturers/create">Add Manufacturer</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" aria-current="page" to="/models/">Vehicle Models </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" aria-current="page" to="/models/create">Add Model </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" aria-current="page" to="/automobiles/">Automobiles</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" aria-current="page" to="/automobiles/create"> Add Automobiles</NavLink>
            </li>
          </ul>
          </li>
          <li className="nav-item dropdown">
          <button className="btn btn-dark dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
            Sales
          </button>
          <ul className="dropdown-menu dropdown-menu-dark">
          <li className="nav-item">
              <NavLink className="nav-link" aria-current="page" to="salerecords/">Sales Records</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" aria-current="page" to="salerecords/create/">Add Sales Record</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" aria-current="page" to="salespersons/create/">Add Sales Person </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" aria-current="page" to="customers/create/">Add Customer </NavLink>
            </li>
          </ul>
          </li>
          <li className="nav-item dropdown">
          <button className="btn btn-dark dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
            Service
          </button>
          <ul className="dropdown-menu dropdown-menu-dark">
          <li className="nav-item">
              <NavLink className="nav-link" aria-current="page" to="technicians/new">Technicians</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" aria-current="page" to="appointments/new">Create Appointment</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" aria-current="page" to="appointments">Scheduled Appointments </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" aria-current="page" to="appointments/history">Service History </NavLink>
            </li>
          </ul>
          </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
