import React, { useEffect, useState } from 'react';

const AddSaleRecordForm = () => {
    const [salespersons, setSalesPersons] = useState([])
    const [customers, setCustomers] = useState([])
    const [automobilevos, setAutomobileVOs] = useState([])
    const [formData, setFormData] = useState({
        sale_price: '',
        sales_person: {},
        customer: {},
        automobile: {},
    })


    const fetchData = async () => {
        const response1 = await fetch("http://localhost:8090/api/salespersons/");
        const response2 = await fetch("http://localhost:8090/api/customers/");
        const response3 = await fetch("http://localhost:8090/api/automobilevos/");
        if (response1.ok && response2.ok && response3.ok) {
            const data1 = await response1.json();
            const data2 = await response2.json();
            const data3 = await response3.json();
            setSalesPersons(data1.salesperson);
            setCustomers(data2.customer);
            setAutomobileVOs(data3.automobilevo);
        }
    }

    useEffect(() => {
        fetchData();
    }, [])

    const handleSubmit = async (event) => {
        event.preventDefault()
        const url = "http://localhost:8090/api/salerecords/"

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
              'Content-Type': 'application/json',
            },
          };
            const response = await fetch(url, fetchConfig);

          if (response.ok) {
            setFormData({
                sale_price: '',
                sales_person: {},
                customer: {},
                automobile: {},
            });
          }


        }

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({...formData, [inputName]: value});
    }

    return (
        <div className="row">
              <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                  <h1>Add a Sale Record</h1>
                  <form onSubmit={handleSubmit} id="create-salerecord-form">
                    <div className="form-floating mb-3">
                      <input onChange={handleFormChange} value={formData.sale_price} placeholder="sale_price" required type="number" name="sale_price" id="sale_price" className="form-control" />
                      <label htmlFor="sale_price">Sale Price</label>
                    </div>
                    <div className="form-floating mb-3">
                    <select onChange={handleFormChange} values={formData.sales_person} required name="salesperson" id="salesperson" className="form-select">
                        <option value="">Choose a Sales Person</option>
                        {salespersons.map(sales_person => {
                          return (
                            <option key={sales_person.id} value={sales_person.id}>{sales_person.name} --- Emp# {sales_person.employee_number}</option>
                          )
                        })}
                      </select>
                    </div>
                    <div className="form-floating mb-3">
                        <select onChange={handleFormChange} values={formData.customer} required name="customer" id="customer" className="form-select">
                        <option value="">Choose a Customer</option>
                        {customers.map(customer => {
                          return (
                            <option key={customer.id} value={customer.id}>{customer.name} --- PH# {customer.phone_number}</option>
                          )
                        })}
                        </select>
                    </div>
                    <div className="form-floating mb-3">
                        <select onChange={handleFormChange} required name="automobilevo" id="automobilevo" className="form-select">
                        <option value="">Choose an Automobile</option>
                        {automobilevos.map(automobilevo => {
                          return (
                            <option key={automobilevo.id} value={automobilevo.id}>{automobilevo.year} {automobilevo.color } {automobilevo.model} --- {automobilevo.vin}</option>
                          )
                        })}
                        </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                  </form>
                </div>
              </div>
            </div>
          )
        }

    export default AddSaleRecordForm;
