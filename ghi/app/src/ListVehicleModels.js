import { useState, useEffect} from 'react'

const ListVehicleModels = () => {
    const [models, setModels] = useState([])

    const getData = async () => {
        const resp = await fetch('http://localhost:8100/api/models/')
        if (resp.ok) {
            const data = await resp.json()
            setModels(data.models)
        }
    }

    useEffect(()=> {
        getData()
    }, [])


    return <>
        <div className="">
            <div className="offset-3 col-9">
                <div className="shadow p-6 mt-5">
                    <h1>Vehicle Models</h1>
                    <table className="table table-dark">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Manufacturer</th>
                                <th>Picture</th>
                            </tr>
                        </thead>

                        <tbody>
                                {models.map(model => {
                                    return (
                                        <tr key={ model.id }>
                                            <td>{ model.name }</td>
                                            <td>{ model.manufacturer.name }</td>
                                            <td><img src={ model.picture_url } height="150" width="auto"/></td>
                                        </tr>
                                    );
                                })}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </>
}

export default ListVehicleModels;
