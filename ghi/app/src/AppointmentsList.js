import { useEffect, useState } from 'react';


function AppointmentsList() {
    const [appointments, setAppointments] = useState([]);

    const getData = async () => {
     const response = await fetch('http://localhost:8080/service_rest/appointments/');

     if (response.ok) {
        const data = await response.json();
        setAppointments(data.appointments)
     }
    }


    useEffect(() => {
        getData()
    }, []);


    const handleDelete = async (e) => {
        const url = `http://localhost:8080/service_rest/appointments/${e.target.id}`;

        const fetchConfigs = {
            method: "Delete",
            header: {
                "Content-Type": "application/json"
            }
        }

        const response = await fetch(url, fetchConfigs)


        setAppointments(appointments.filter(appointment => String(appointment.pk) !== e.target.id))
    }


    return (
        <table className="table table-striped">
          <thead>
            <tr>
              <th>VIN</th>
              <th>Customer Name</th>
              <th>Date-Time</th>
              <th>Technician</th>
              <th>Reason</th>
            </tr>
          </thead>
          <tbody>
            {appointments.map(appointment => {
              return (
                <tr key={appointment.pk}>
                  <td>{ appointment.vin.vin }</td>
                  <td>{ appointment.name }</td>
                  <td>{appointment.date_time}</td>
                  <td>{appointment.technician.name}</td>
                  <td>{appointment.reason_for_service}</td>
                  <td><button onClick={handleDelete} id={appointment.pk} className="btn btn-danger">Cancel</button></td>
                  <td><button onClick={handleDelete} id={appointment.pk} className="btn btn-success">Completed</button></td>
                </tr>
              );
            })}
          </tbody>
        </table>
      );
}
export default AppointmentsList;
