import React, { useEffect, useState } from 'react'


function CreateAppointmentForm () {
    const [technicians, setTechnicians] = useState([]);


    const getData = async () => {
        const url = 'http://localhost:8080/service_rest/technicians/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setTechnicians(data.technician);
        }
    }

    useEffect(() => {
        getData();
    }, []);


    const [formData, setFormData] = useState({
        name: '',
        date_time: '',
        reason_for_service: '',
        technician: '',
        vin: '',
    })


    const handleSubmit = async (event) => {
        event.preventDefault();

        const url = 'http://localhost:8080/service_rest/appointments/';

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(url, fetchConfig);

        if (response.ok) {
            setFormData({
                name: '',
                date_time: '',
                reason_for_service: '',
                technician: '',
                vin: '',
            });
        }
    }


    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;

        setFormData({
            ...formData,
            [inputName]: value
        });
    }

    return (

        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Enter a Service Appointment</h1>
            <form onSubmit={handleSubmit} id="create-appointment-form">
              <div className="form-floating mb-3">
                <input value={formData.vin} onChange={handleFormChange} placeholder="Vin Number" required type="text" name="vin" id="vin" className="form-control"/>
                <label htmlFor="vin">Vin Number</label>
              </div>
              <div className="form-floating mb-3">
                <input value={formData.name} onChange={handleFormChange} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input value={formData.date_time} onChange={handleFormChange} placeholder="Date & Time" required type="datetime-local" name="date_time" id="date_time" className="form-control"/>
                <label htmlFor="date_time">Date & Time</label>
              </div>
              <div className="mb-3">
                <select value={formData.technician} onChange={handleFormChange} required name="technician" id="technician" className="form-select">
                  <option value="">Technician</option>
                  {technicians.map(technician => {
                  return (
                    <option key={technician.pk} value={technician.pk}>
                      Technician: {technician.name} -- Employee Number: {technician.employee_num}
                    </option>
                  );
                })}
                </select>
              </div>
              <div className="form-floating mb-3">
                <input value={formData.reason_for_service} onChange={handleFormChange} placeholder="Reason For Service" required type="text" name="reason_for_service" id="reason_for_service" className="form-control"/>
                <label htmlFor="reason_for_service">Reason For Service</label>
              </div>
              <button className="btn btn-primary">Enter A Service Appointment</button>
            </form>
          </div>
        </div>
      </div>
    )
}
export default CreateAppointmentForm;
