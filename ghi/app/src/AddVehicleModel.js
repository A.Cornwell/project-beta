import React, { useEffect, useState } from 'react';
import './AddVehicleModel.css';


function AddVehicleModel() {

    const [manufacturers, setManufacturers] = useState([]);
    const fetchData = async () => {
        const url = "http://localhost:8100/api/manufacturers/"
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json()
            setManufacturers(data.manufacturers)
        }
    }

    const [name, setName] = useState("")
    const handleName = (event) => {
        const value = event.target.value;
        setName(value)
    }
    const [picture_url, setPicture_URL] = useState("")
    const handlePicture_URL = (event) => {
        const value = event.target.value;
        setPicture_URL(value)
    }
    const [manufacturer, setManufacturer] = useState("")
    const handleManufacturer = (event) => {
        const value = event.target.value;
        setManufacturer(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = {}
        data.name = name
        data.picture_url = picture_url
        data.manufacturer = manufacturer
        const url = "http://localhost:8100/api/models/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
              'Content-Type': 'application/json',
            },
        };
        const response = await fetch(url, fetchConfig);

        if (response.ok) {
            setName("");
            setPicture_URL("");
            setManufacturer("");
        }
    }

        useEffect(() => {
            fetchData();
        }, [])

            return (
            <div className="vehicle">
              <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                  <h1>Add a Vehicle Model</h1>
                  <form onSubmit={handleSubmit} id="create-model-form">
                  <div className="form-floating mb-3">
                    <input onChange={handleName} value={name} placeholder="Name" required type="text" className="form-control" id="name" name="name"  />
                      <label id="vehicle" htmlFor="name">Name</label>
                    </div>
                  <div className="form-floating mb-3">
                    <input onChange={handlePicture_URL} value={picture_url} required type="text" placeholder="Picture URL" className="form-control" id="picture_url" name="picture_url"  />
                      <label id="vehicle" htmlFor="picture_url">Picture URL</label>
                    </div>
                    <div className="form-floating mb-3">
                    <select onChange={handleManufacturer} value={manufacturer.id} required name="manufacturer" id="manufacturer" className="form-select">
                        <option value="">Choose a manufacturer</option>
                        {manufacturers.map(manufacturer => {
                          return (
                            <option key={manufacturer.id} value={manufacturer.id}>{manufacturer.name}</option>
                          )
                        })}
                      </select>
                    </div>
                        <button className="vehicle-btn btn-dark">Create</button>
                  </form>
                </div>
              </div>
            </div>
          )
        }

        export default AddVehicleModel;
