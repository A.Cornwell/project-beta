import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import CreateTechnicianForm from './CreateTechnicianForm';
import AppointmentsList from './AppointmentsList';
import ServiceHistory from './ServiceHistory';
import CreateAppointmentForm from './CreateAppointmentForm';
import AddSaleRecordForm from './AddSaleRecordForm';
import AddSalesPersonForm from './AddSalesPersonForm';
import AddPotentialCustomerForm from './AddPotentialCustomerForm';
import SaleRecordList from './SaleRecordList';
import ListVehicleModels from './ListVehicleModels'
import Manufacturers from './ManufacturersList';
import CreateManufacturer from './CreateManufacturer';
import AddVehicleModel from './AddVehicleModel';
import AutomobilesList from './AutomobilesList';
import CreateAutomobile from './CreateAutomobile.js';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
            <Route path="/" element={<MainPage />} />
            <Route path="manufacturers/create" element={<CreateManufacturer />} />
            <Route path="manufacturers" element={<Manufacturers />} />
            <Route path="models/create/" element={<AddVehicleModel />} />
            <Route path="models/" element={<ListVehicleModels />} />
            <Route path="automobiles/create" element={<CreateAutomobile />} />
            <Route path="automobiles/" element={<AutomobilesList />} />
            <Route path="technicians/new" element={<CreateTechnicianForm />} />
            <Route path="appointments/new" element={<CreateAppointmentForm />} />
            <Route path="appointments" element={<AppointmentsList />} />
            <Route path="appointments/history" element={<ServiceHistory />} />
            <Route path="salerecords/" element={<SaleRecordList />} />
            <Route path="salerecords/create/" element={<AddSaleRecordForm />} />
            <Route path="salespersons/create/" element={<AddSalesPersonForm />} />
            <Route path="customers/create/" element={<AddPotentialCustomerForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
