import React, { useState } from 'react';


function CreateTechnicianForm () {


    const [formData, setFormData] = useState({
        name: '',
        employee_num: '',
    })


    const handleSubmit = async (event) => {
        event.preventDefault();

        const url = 'http://localhost:8080/service_rest/technicians/';

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(url, fetchConfig);

        if (response.ok) {
            setFormData({
                name: '',
                employee_num: '',
            });
        }
    }

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;

        setFormData({
            ...formData,
            [inputName]: value
        });
    }


    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Enter a technician</h1>
            <form onSubmit={handleSubmit} id="create-technician-form">
              <div className="form-floating mb-3">
                <input value={formData.name} onChange={handleFormChange} placeholder=" Name " required type="text" name="name" id="name" className="form-control"/>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input value={formData.employee_num} onChange={handleFormChange} placeholder="Employee Number" required type="text" name="employee_num" id="employee_num" className="form-control"/>
                <label htmlFor="employee_num">Employee Number</label>
              </div>
              <button className="btn btn-primary">Enter Technician</button>
            </form>
          </div>
        </div>
      </div>
    )


}
export default CreateTechnicianForm;
