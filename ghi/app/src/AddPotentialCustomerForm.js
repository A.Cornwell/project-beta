import React, { useEffect, useState } from 'react';


function AddPotentialCustomerForm () {
    const [customers, setCustomers] = useState([]);

    const [formData, setFormData] = useState({
        name: '',
        phone_number: '',
        address_1: '',
        address_2: '',
        city: '',
        state: '',
        zip_code: '',
    })

    const handleSubmit = async (event) => {
        event.preventDefault();

        const url = 'http://localhost:8090/api/customers/';

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(url, fetchConfig);

        if (response.ok) {
            setFormData({
                name: '',
                phone_number: '',
                address_1: '',
                address_2: '',
                city: '',
                state: '',
                zip_code: '',
            });
        }
    }

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;

        setFormData({
            ...formData,
            [inputName]: value
        });
    }


    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a Potential Customer</h1>
            <form onSubmit={handleSubmit} id="create-customer-form">
              <div className="form-floating mb-3">
                <input value={formData.name} onChange={handleFormChange} placeholder="Name" required type="text" maxLength={100} name="name" id="name" className="form-control"/>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input value={formData.phone_number} onChange={handleFormChange} placeholder="Phone Number" required type="number" name="phone_number" id="phone_number" className="form-control"/>
                <label htmlFor="phone_number">Phone Number</label>
              </div>
              <div className="form-floating mb-3">
                <input value={formData.address_1} onChange={handleFormChange} placeholder="Address 1" required type="" name="address_1" maxLength={128} id="address_1" className="form-control"/>
                <label htmlFor="address_1">Address 1</label>
              </div>
              <div className="form-floating mb-3">
                <input value={formData.address_2} onChange={handleFormChange} placeholder="Address 2" name="address_2" maxLength={128} id="address_2" className="form-control"/>
                <label htmlFor="address_2">Address 2 (Optional)</label>
              </div>
              <div className="form-floating mb-3">
                <input value={formData.city} onChange={handleFormChange} placeholder="City" required type="text" name="city" id="city" maxLength={64} className="form-control"/>
                <label htmlFor="city">City</label>
              </div>
              <div className="form-floating mb-3">
                <input value={formData.state} onChange={handleFormChange} placeholder="State" required type="text" name="state" maxLength={2} id="state" className="form-control"/>
                <label htmlFor="state">State</label>
              </div>
              <div className="form-floating mb-3">
                <input value={formData.zip_code} onChange={handleFormChange} placeholder="Zipcode" required type="number" name="zip_code" maxLength={5} id="zip_code" className="form-control"/>
                <label htmlFor="zip_code">Zipcode</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )


}
export default AddPotentialCustomerForm;
