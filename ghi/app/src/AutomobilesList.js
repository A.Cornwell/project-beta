import { useState, useEffect} from 'react'
import './AutomobilesList.css'

const AutomobilesList = () => {
    const [autos, setAutos] = useState([])

    const getData = async () => {
        const resp = await fetch('http://localhost:8100/api/automobiles/')
        if (resp.ok) {
            const data = await resp.json()
            setAutos(data.autos)
        }
    }

    useEffect(()=> {
        getData()
    }, [])


    return <>
        <div className="row">
            <div className="offset-3 col-9">
                <div className="shadow p-6 mt-5">
                    <h1>Available Automobiles</h1>
                    <table className="table table-dark">
                        <thead>
                            <tr>
                                <th>VIN</th>
                                <th>Color</th>
                                <th>Year</th>
                                <th>Model</th>
                                <th>Manufacturer</th>
                            </tr>
                        </thead>

                        <tbody>
                                {autos.filter(auto => auto.is_sold === false).map(auto => {
                                    return (
                                        <tr key={ auto.id }>
                                            <td>{ auto.vin }</td>
                                            <td>{ auto.color }</td>
                                            <td>{ auto.year }</td>
                                            <td>{ auto.model.name }</td>
                                            <td>{ auto.model.manufacturer.name }</td>
                                        </tr>
                                    );
                                })}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </>
}

export default AutomobilesList;
