import React, { useEffect, useState } from 'react'
import './CreateAutomobile.css'

function CreateAutomobile () {
    const [models, setModels] = useState([]);


    const getData = async () => {
        const url = 'http://localhost:8100/api/models/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setModels(data.models);

        }
    }

    useEffect(() => {
        getData();
    }, []);


    const [formData, setFormData] = useState({
        color: '',
        year: '',
        vin: '',
        model: '',
    })


    const handleSubmit = async (event) => {
        event.preventDefault();

        const url = 'http://localhost:8100/api/automobiles/';

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(url, fetchConfig);

        if (response.ok) {
            setFormData({
                color: '',
                year: '',
                vin: '',
                model: '',
            });
        }
    }


    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;

        setFormData({
            ...formData,
            [inputName]: value
        });
    }


    return (

        <div className="auto">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add Automobile</h1>
            <form onSubmit={handleSubmit} id="create-automobile-form">
            <div className="form-floating mb-3">
                <input value={formData.color} onChange={handleFormChange} placeholder="Color" required type="text" name="color" id="color" className="form-control"/>
                <label id="auto" htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input value={formData.year} onChange={handleFormChange} placeholder="Year" required type="text" name="year" id="year" className="form-control"/>
                <label id="auto" htmlFor="year">Year</label>
              </div>
              <div className="form-floating mb-3">
                <input value={formData.vin} onChange={handleFormChange} placeholder="VIN" required type="text" name="vin" id="vin" className="form-control"/>
                <label id="auto" htmlFor="vin">VIN</label>
              </div>
              <div className="mb-3">
                <select id="autoselect" value={formData.model} onChange={handleFormChange} required name="model" id="model" className="form-select">
                  <option value="">Model</option>
                  {models.map(model => {
                  return (
                    <option key={model.href} value={model.href}>
                      Model: {model.name}
                    </option>
                  );
                })}
                </select>
              </div>
              <button className="btn btn-dark auto-btn">Add Automobile</button>
            </form>
          </div>
        </div>
      </div>
    )
}
export default CreateAutomobile;
