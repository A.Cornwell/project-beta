import { Link } from 'react-router-dom'
import { useState, useEffect} from 'react'

const SaleRecordListEmployee = () => {
    const [salerecords, setSaleRecords] = useState([])

    const getData = async () => {
        const resp = await fetch(`http://localhost:8090/api/salerecords/`)
        if (resp.ok) {
            const data = await resp.json()
            setSaleRecords(data.salerecords)
        }
    }

    useEffect(()=> {
        getData()
    }, [])


    return <>
        <div className="row">
            <div className="offset-3 col-9">
                <div className="shadow p-6 mt-4">
                    <h1>Sale Records</h1>
                    <table className="table table-striped">
                        <thead>
                            <tr>
                                <th>Sales Person</th>
                                <th>Customer</th>
                                <th>VIN</th>
                                <th>Sale price</th>
                                <th></th>
                            </tr>
                        </thead>

                        <tbody>
                            {
                                salerecords.map(salerecord => {
                                    return (
                                    <tr key={salerecord.id}>
                                        <td><Link to={`/salerecords/${salerecord.sales_person.id}/`}>{ salerecord.sales_person.name }</Link></td>
                                        <td>{ salerecord.customer.name }</td>
                                        <td>{ salerecord.automobile.vin }</td>
                                        <td>{ salerecord.sale_price }</td>
                                        {/* <td><Link to={`/locations/${location.id}`}>{ location.name }</Link></td> */}
                                        {/* <td><button onClick={handleDelete} id={location.id} className="btn btn-danger">Delete</button></td> */}
                                    </tr>
                                    );
                                })
                            }
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </>
}

export default SaleRecordListEmployee;
