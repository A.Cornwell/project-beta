import { useEffect, useState } from 'react';


function ServiceHistory() {
  const [appointments, setAppointments] = useState([]);
  const [filterTerm, setFilterTerm] = useState("vin");

  const getData = async () => {
    const response = await fetch('http://localhost:8080/service_rest/appointments/');

    if (response.ok) {
      const data = await response.json();
      const appointments = data.appointments.map((appointment) => {
        return {
          vin: appointment.vin,
          name: appointment.name,
          date_time: appointment.date_time,
          technician: appointment.technician,
          reason_for_service: appointment.reason_for_service
        };
      });

      setAppointments(appointments);


    }
  };


  useEffect(() => {
    getData();
  }, []);


  const handleFilterChange = (e) => {
    setFilterTerm(e.target.value);
  }


  return (
    <div>
      <br />
      <input onChange={handleFilterChange} className="form-control mr-sm-2" type="search" placeholder="Search VIN" aria-label="Search" />
      <br />
      <h1>Service History</h1>
      <hr />
      <table className="table table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Customer Name</th>
            <th>Date-Time</th>
            <th>Technician</th>
            <th>Reason</th>
          </tr>
        </thead>
        <tbody>
          {appointments
            .filter((appointment) => appointment.vin.vin.includes(filterTerm))
            .map((appointment) => {
              return (
                <tr key={appointment.id}>
                  <td>{appointment.vin}</td>
                  <td>{appointment.name}</td>
                  <td>{appointment.date_time}</td>
                  <td>{appointment.technician}</td>
                  <td>{appointment.reason_for_service}</td>
                </tr>
              );
            })}
        </tbody>
      </table>
    </div>
  );

}


export default ServiceHistory;
