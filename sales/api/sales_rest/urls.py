from django.urls import path
from .views import api_sales, api_salespersons, api_customer, api_automobilevo

urlpatterns = [
    path("salerecords/", api_sales, name="api_sales"),
    path("salerecords/<int:sales_person_id>/", api_sales, name="api_person_sales"),
    path("salerecords/create/", api_sales, name="api_create_sale"),
    path("salespersons/", api_salespersons, name="api_salespersons" ),
    path("customers/", api_customer, name="api_customer"),
    path("automobilevos/", api_automobilevo, name="api_automobilevo"),
]
