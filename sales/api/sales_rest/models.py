
from django.db import models


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    year = models.PositiveSmallIntegerField()
    model = models.CharField(max_length=100)
    color = models.CharField(max_length=50)
    is_sold = models.BooleanField(default=False)

    def __str__(self):
        return self.vin


class SalesPerson(models.Model):
    name = models.CharField(max_length=100)
    employee_number = models.PositiveSmallIntegerField(unique=True)

    def __str__(self):
        return self.name


class Customer(models.Model):
    name = models.CharField(max_length=100)
    phone_number = models.PositiveBigIntegerField()
    address_1 = models.CharField(max_length=128)
    address_2 = models.CharField(max_length=128, blank=True)
    city = models.CharField(max_length=64)
    state = models.CharField(max_length=2)
    zip_code = models.CharField(max_length=5)

    def __str__(self):
        return self.name


class SaleRecord(models.Model):
    sale_price = models.PositiveIntegerField()
    sales_person = models.ForeignKey(SalesPerson, related_name="salerecord", on_delete=models.PROTECT)
    customer = models.ForeignKey(Customer, related_name="salerecord", on_delete=models.PROTECT)
    automobile = models.ForeignKey(AutomobileVO, related_name="salerecord", on_delete=models.PROTECT)
