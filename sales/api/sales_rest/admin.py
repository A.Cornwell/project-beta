from django.contrib import admin
from .models import SalesPerson, Customer, SaleRecord, AutomobileVO

@admin.register(AutomobileVO)
class AutomobileVO(admin.ModelAdmin):
    pass

@admin.register(SalesPerson)
class SalesPerson(admin.ModelAdmin):
    pass

@admin.register(Customer)
class Customer(admin.ModelAdmin):
    pass

@admin.register(SaleRecord)
class SaleRecord(admin.ModelAdmin):
    pass
