from django.shortcuts import render
from .models import AutomobileVO, SalesPerson, Customer, SaleRecord
import json
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from .encoders import AutomobileVOEncoder, CustomerEncoder, SalesPersonEncoder, SaleRecordListEncoder


@require_http_methods(["GET", "POST"])
def api_sales(request, sales_person_id=None):
    if request.method == "GET":
        if sales_person_id == None:
            salerecords = SaleRecord.objects.all()
            return JsonResponse({"salerecords": salerecords}, encoder=SaleRecordListEncoder, safe=False)
        else:
            salerecords = SaleRecord.objects.filter(sales_person_id=sales_person_id)
            return JsonResponse({"salerecords": salerecords}, encoder=SaleRecordListEncoder, safe=False)
    else:
        try:
            content = json.loads(request.body)
            salerecord = SaleRecord.objects.create(**content)
            return JsonResponse(
                {"salerecord": salerecords}, encoder=SaleRecordListEncoder, safe=False
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the sale record"}
            )
            response.status_code = 400
            return response


@require_http_methods(["GET", "POST"])
def api_salespersons(request):
    if request.method == "GET":
        salespersons = SalesPerson.objects.all()
        return JsonResponse(
            {"salesperson": salespersons},
            encoder = SalesPersonEncoder,
        )
    else:
        content = json.loads(request.body)
        salesperson = SalesPerson.objects.create(**content)
        return JsonResponse(
            salesperson,
            encoder = SalesPersonEncoder,
            safe=False,
        )

@require_http_methods(["GET", "POST"])
def api_customer(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customer": customers},
            encoder = CustomerEncoder,
        )
    else:
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder = CustomerEncoder,
            safe=False,
        )


@require_http_methods(["GET"])
def api_automobilevo(request):
    automobilevos = AutomobileVO.objects.all()
    return JsonResponse(
        {"automobilevo": automobilevos},
        encoder = AutomobileVOEncoder,
    )
