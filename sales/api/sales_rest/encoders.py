from common.json import ModelEncoder
from .models import AutomobileVO, SalesPerson, Customer, SaleRecord

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "year",
        "model",
        "color",
        "is_sold",
        "id",
    ]

class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "name",
        "phone_number",
        "address_1",
        "address_2",
        "city",
        "state",
        "zip_code",
        "id",
    ]

class SalesPersonEncoder(ModelEncoder):
    model = SalesPerson
    properties = [
        "name",
        "employee_number",
        "id"
    ]

class SaleRecordListEncoder(ModelEncoder):
    model = SaleRecord
    properties = [
        "sale_price",
        "sales_person",
        "customer",
        "automobile",
        "id",
    ]
    encoders = {
        "automobile": AutomobileVOEncoder(),
        "sales_person": SalesPersonEncoder(),
        "customer": CustomerEncoder(),
    }
