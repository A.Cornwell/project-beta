# CarCar

Team:

* Aaron Cornwell - Service
* Alexander Drosos - Sales

## Design
designed with boostrap

## Service microservice

Explain your models and integration with the inventory
microservice, here.

The appointment, technician, and automobileVO models within the "service" bounded context were integrated with the inventory microservice to allow us to create and update appointments and technicians. Technicians can be created and assigned to a specific appoinmtent. Appointments can be created and displayed on our page for active appointments.

## Sales microservice

To complete the outlined Sales microservice goals, I felt it necessary to create models for SalesPerson, Customer, SaleRecord, and a VO for Automobile. This takes advantage of a poller to pull current automobile information from the inventory service and store it within the VO. Additionally, I added an "is_sold" attribute to the Inventory Automobile model that also polls to the VO. This allows us to show only is_sold=false automobiles on the automobiles page.
Successful creation of all of my models allowed me to have dynamic choices within my Create Sale Record form.
